#include <stdio.h>
#include <stdlib.h>

#include "classes/genenode.h"
#include "classes/genelist.h"
#include "classes/gene.h"
#include "classes/pool.h"
#include "classes/fragment.h"

int main(int argc, char *argv[]) {
  FILE* input;
  FILE* gene_input;
  int num_fragments;
  int num_genes;
  int num_crosses;
  float mutation_probability;
  int first_nallow;
  int first_allow[100];
  int p[6],base_index[7];
  char junk[6]="";
  fragment** frags;

/*intialize the number of genes, number of root base and what is root base*/
  num_genes=1;
  first_nallow=1;
  first_allow[0]=0;
  num_crosses=3;
  mutation_probability=0.1;
/*Done initialization number of genes, number of root base and what is root base*/

/*Reading fragments input files frag.inp*/
  input = fopen("frag.inp", "r");
  fscanf(input, "%i%s", &num_fragments, junk);
  printf("Total number of fragments: %i\n", num_fragments);
  frags= (fragment**)malloc(num_fragments*sizeof(fragment*));
  for (int ifrag=0; ifrag<num_fragments; ifrag++) {
    frags[ifrag]=new fragment;
    if ((frags[ifrag])->read_from_file(input, ifrag)) return -1;
    printf("Reading Fragment%i successfully, has %i atoms\n", ifrag, frags[ifrag]->get_num_atoms());
    printf("\n");
  }
  printf("Done with fragments reading from frag.inp\n");
  printf("--------------------------------------------------------------------------- \n");
  printf("\n");
/*Done reading fragments input files frag.inp*/

  pool* poo = new pool(num_genes, num_crosses, mutation_probability, frags, first_nallow, first_allow);

/*Reading Gene files*/
  for (int igene=0; igene < num_genes; igene++){
    char file_name_igene[20];
    sprintf(file_name_igene, "file_with_bits%i.txt", igene);
    gene_input = fopen(file_name_igene, "r");
    fscanf(gene_input, "%i,%i,%i,%i,%i,%i", &p[0], &p[1], &p[2], &p[3], &p[4], &p[5]);
    base_index[0] = 0;
    /*Convert the string to indices of fragments*/
    for (int i=0; i<6; i++) {
      if (p[i] == 00) {
        base_index[i+1] = 1;
      }
      else if (p[i] == 01) {
        base_index[i+1] = 2;
      }
      else if (p[i] == 11) {
        base_index[i+1] = 3;
      }
      else if (p[i] == 10) {
        base_index[i+1] = 4;
      }
      printf("%i, %i\n", p[i], base_index[i+1]);
    }
    /*Done Convert (it is done in a stupid way, could be changed later*/
   poo->initialize_random(igene, base_index);
   poo->print_pool(igene);
   poo->initialize_zmatrix(igene);
  }
/*Done reading Gene files*/

}

