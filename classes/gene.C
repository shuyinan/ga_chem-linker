#include "gene.h"

#include "genelist.h"

gene::gene() {
  first_base=NULL;
  mutation_probability=-1.0;
  fragments=NULL;
}

gene::gene(fragment** frags) {
  first_base=NULL;
  mutation_probability=-1.0;
  fragments=frags;
}

gene::gene(double mutprob, fragment** frags, int fnav, int* nav) {
  first_base=NULL;
  mutation_probability=mutprob;
  fragments=frags;
  first_num_allowed_values=fnav;
  for (int iallow=0; iallow<fnav; iallow++) {
    first_allowed_values[iallow]=nav[iallow];
  }
}

gene::~gene() {
  delete first_base;
}

void gene::get_bases(int* bases) {
  int nbases = get_num_bases();
  int num=0;
  first_base->get_my_base(bases, num);
}

fragment** gene::get_fragments() {
  return fragments;
}

void gene::set_bases(int* bases) {
  //printf("set_bases a\n");  
  if (first_base != NULL) {
    delete first_base;
    first_base=NULL;
  }
  //printf("set_bases b\n");  
  first_base=new base(bases[0], fragments);
  //printf("set_bases c\n");  
  int num=0;
  first_base->add_my_bases(bases, num);
  //printf("set_bases d\n");  
}

void gene::copy(gene* to_be_copied){
  //printf("copy a %i\n", first_base);
  fragments=to_be_copied->get_fragments();
  if (first_base != NULL) {
    delete first_base;
    first_base=NULL;
  }
  //printf("copy b\n");
  int nbases = to_be_copied->get_num_bases();
  int* bases;
  bases=(int*)malloc(nbases*sizeof(int));
  //printf("copy c\n");
  to_be_copied->get_bases(bases);
  //printf("copy d %i ", nbases);
  //for (int i=0; i<nbases; i++) printf("%i ", bases[i]);
  //printf("\n");
  set_bases(bases);
  //printf("copy e\n");
  free(bases);
  //printf("copy f\n");
  mutation_probability=to_be_copied->get_mutation_probability();
  int nfav=to_be_copied->get_first_num_allowed_values();
  //printf("copy gene, %i\n", nfav);
  int fav[nfav];
  to_be_copied->get_first_allowed_values(fav);
  set_first_allowed_values(nfav, fav);
}

//not done, need to add allowed bases
void gene::mutate(int fnav, int* fav) {
   
  //printf("fnav %i\n", first_num_allowed_values);
  first_num_allowed_values = fnav;
  for ( int first_allow=0; first_allow < fnav; first_allow++) {
  first_allowed_values[first_allow] = fav[first_allow];
  }
  // first_base->mutate_me(mutation_probability, fnav, fav);
  first_base->mutate_me(mutation_probability, first_num_allowed_values, first_allowed_values);
  //printf("fnav %i returned\n", first_num_allowed_values);
  
  /*  for (int ibase = 0; ibase < num_bases; ibase++) {
    double random_number = (double)rand() / (double)RAND_MAX;
    if (random_number < mutation_probability) {
      bases[ibase] = rand() % max_values[ibase];
    }
    }*/
}

void gene::initialize_random(int val, int* base_index) {
  first_base = new base(val,fragments);
  first_base->initialize_random_children(base_index);
}
  
void gene::print_gene() {
  first_base->print_me();
  printf(" ");
}

void gene::print_gene_restart() {
  first_base->print_me_restart_total();
  first_base->print_me_restart();
  FILE* check = fopen("check.dat", "a+");
  fprintf(check,  "\n ");
  fclose(check);
}
  
int gene::get_num_bases() {
  int num=0;
  num=first_base->count_me(num);
  return num;
}

double gene::get_mutation_probability() {
  return mutation_probability;
}

void gene::set_mutation_probability(double mutprob) {
  mutation_probability = mutprob;
}

void gene::set_first_allowed_values(int first_nallow, int* first_allow) {
  first_num_allowed_values=first_nallow;
  for (int i=0; i<first_num_allowed_values; i++) {
    //printf("sfav %i %i %i\n",i, first_allow[i]);
    first_allowed_values[i]=first_allow[i];
  }
}

int gene::get_first_num_allowed_values() {
  return first_num_allowed_values;
}

void gene::get_first_allowed_values(int* first_allow) {
  for (int i=0; i<first_num_allowed_values; i++) {
    //printf("gfav %i %i %i\n",i, first_allow[i]);
    first_allow[i]=first_allowed_values[i];
  }
}

base* gene::get_base_by_index(int ibase) {
  if (ibase==0) {
    return first_base;
  }
  int num=0;
  base* base_out;
  num = first_base->getting_base_by_index(ibase, num, &base_out);
  return base_out;
}

void gene::replace_base_by_index(int ibase, base* base_in) {
  if (ibase==0) {
    first_base=base_in;
  }
  int num=0;
  //base* base_out;
  num = first_base->replacing_base_by_index(ibase, num, base_in);
}

int gene::allows_base_by_index(int ibase, base* base_in) {
  if (ibase==0) {
    for (int ifrag=1; ifrag < first_num_allowed_values; ifrag++) {
      if (first_allowed_values[ifrag] == base_in->get_value()) return 1;
    }
    return 0;
  }
  int num=0;
  int ret=0;
  num = first_base->checking_allowed_base_by_index(ibase, num, base_in, &ret);
  return ret;
}

void gene::crossover_with(gene* gene2) {
  int nbases1 = get_num_bases();
  int nbases2 = gene2->get_num_bases();
  if (nbases1 == 1 || nbases2 == 1) return;
  int irand1 = 1 + rand() % (nbases1-1);
  int irand2 = 1 + rand() % (nbases2-1);
  printf("The two parent genes are:");
  print_gene();
  printf("; ");
  gene2->print_gene();
  printf(" \n");
  printf("Crossover possitions: Site %i ; Site %i\n",(irand1-1), (irand2-1));
  base* base1 = get_base_by_index(irand1);
  base* base2 = gene2->get_base_by_index(irand2);
 
  printf("Checking crossover availability: ");
  if (allows_base_by_index(irand1, base2) && 
      gene2->allows_base_by_index(irand2, base1) ) {
    printf("crossover allowed; ");
    replace_base_by_index(irand1, base2);
    gene2->replace_base_by_index(irand2, base1);
    printf("crossover complete.\n");
  } else {
    printf("crossover not allowed.\n");
  }
}

int gene::is_less_than(gene* gene2) {
  int nbases1 = get_num_bases();
  int nbases2 = gene2->get_num_bases();
  int* base1;
  int* base2;
  base1=(int*)malloc(nbases1*sizeof(int));
  get_bases(base1);
  base2=(int*)malloc(nbases2*sizeof(int));
  gene2->get_bases(base2);

  if (nbases2<nbases1) nbases1=nbases2;

  int ret=-1;
  for (int ibase=0; ibase<nbases1; ibase++) {
    if (ret==-1 && base1[ibase] < base2[ibase]) {
      ret=1;
    }
    if (ret==-1 && base1[ibase] > base2[ibase]) {
      ret=0;
    }
  }
  if(ret==-1) ret=0;

  /*if (ret) {
    print_gene();
    printf(" is less than ");
    gene2->print_gene();
    printf("\n");
  } else {
    print_gene();
    printf(" is not less than ");
    gene2->print_gene();
    printf("\n");
    }*/
  free(base1);
  free(base2);
  return ret;
}

int gene::is_greater_than(gene* gene2) {
  int nbases1 = get_num_bases();
  int nbases2 = gene2->get_num_bases();
  int* base1;
  int* base2;
  base1=(int*)malloc(nbases1*sizeof(int));
  get_bases(base1);
  base2=(int*)malloc(nbases2*sizeof(int));
  gene2->get_bases(base2);

  if (nbases2<nbases1) nbases1=nbases2;

  int ret=-1;
  for (int ibase=0; ibase<nbases1; ibase++) {
    if (ret==-1 && base1[ibase] < base2[ibase]) {
      ret=0;
    }
    if (ret==-1 && base1[ibase] > base2[ibase]) {
      ret=1;
    }
  }
  if(ret==-1) ret=0;

  /*if (ret) {
    print_gene();
    printf(" is greater than ");
    gene2->print_gene();
    printf("\n");
  } else {
    print_gene();
    printf(" is not greater than ");
    gene2-> print_gene();
    printf("\n");
  }*/
  free(base1);
  free(base2);
  return ret;
}

int gene::is_equal_to(gene* gene2) {
  int nbases1 = get_num_bases();
  int nbases2 = gene2->get_num_bases();
  int* base1;
  int* base2;
  base1=(int*)malloc(nbases1*sizeof(int));
  get_bases(base1);
  base2=(int*)malloc(nbases2*sizeof(int));
  gene2->get_bases(base2);

  if (nbases2<nbases1) nbases1=nbases2;

  int ret=1;
  for (int ibase=0; ibase<nbases1; ibase++) {
    if (base1[ibase] != base2[ibase]) {
      ret=0;
    }
  }

  /*if (ret) {
    print_gene();
    printf(" is equal to ");
    gene2->print_gene();
    printf("\n");
  } else {
    print_gene();
    printf(" is not equal to ");
    gene2->print_gene();
    printf("\n");
    }*/
  free(base1);
  free(base2);
  return ret;
}

void gene::write_zmat(int ng) {

  char file_name_zmat[20];
  sprintf(file_name_zmat, "zmat%i.tmp", ng);
  FILE* zmat = fopen(file_name_zmat,"w");

  int iatom=0;
  iatom = first_base->write_my_zmat(zmat, iatom, 0, 0, 0, 0.0, 0);

  fclose(zmat);  
}
